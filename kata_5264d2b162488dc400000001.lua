-- [ 6 KYU ] - Stop gninnipS My sdroW!
-- (( https://www.codewars.com/kata/5264d2b162488dc400000001/train/lua ))
--

local solution = {}

function solution.spin_words(s)
	local output = {}
	for word in s:gmatch("(%S+)") do
		if #word < 5 then table.insert(output, word) else table.insert(output, string.reverse(word)) end
	end
	return table.concat(output, " ")
end

--print(solution.spin_words("test abcdef"))

return solution
