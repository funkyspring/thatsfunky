-- [ 6 KYU ] - Find The Parity Outlier
-- (( https://www.codewars.com/kata/5526fc09a1bbd946250002dc/train/lua ))
--

FindOutlier = {};
function FindOutlier.find(numbers)
	local odds = 0
	local evens = 0
	for _,n in pairs(numbers) do
		if n % 2 == 0 then odds = odds + 1 else evens = evens + 1 end
		if odds >= 2 and evens == 1 then return n end
		if evens >= 2 and odds == 1 then return n end
	end
end

--print(FindOutlier.find({1,2,1}))
