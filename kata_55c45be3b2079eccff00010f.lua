-- [ 6 KYU ] - Your order,please
-- (( https://www.codewars.com/kata/55c45be3b2079eccff00010f/train/lua ))
--

function order(words)
	local o = {}
	for word in words:gmatch("(%S+)") do
		for c in word:gmatch("(%d)") do
			o[tonumber(c)] = word
			break
		end
	end
	return table.concat(o, " ")
end
--print(order("Thi1s is2 T4est 3a"))
