-- [ 6 KYU ] - Create Phone Number
-- (( https://www.codewars.com/kata/525f50e3b73515a6db000b83/train/lua ))
--

return {
	create_phone_number = function(N)
				return "(" .. N[1] .. N[2] .. N[3] .. ") " .. N[4] .. N[5] .. N[6] .. "-" .. N[7] .. N[8] .. N[9] .. N[10]
	end
}

